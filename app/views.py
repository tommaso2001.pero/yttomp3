import os
from app import app
from flask import send_from_directory, Flask, render_template, request
from urllib import ContentTooShortError
import youtube_dl
@app.route('/', methods=['GET', 'POST'])
def index():
	return render_template("index.html")

@app.route('/ytdl', methods=['GET', 'POST'])
def yt_dl():
	ydl_opts = {
	'format': 'bestaudio/best',
	'verbose': 'true',
	'postprocessors': [{
        'key': 'FFmpegExtractAudio',
        'preferredcodec': 'mp3',
        'preferredquality': '192',
	}]}
	link = request.form['tag']
	with youtube_dl.YoutubeDL(ydl_opts) as ydl:
		if request.method == 'POST':
    			try:
        			ydl.download([link])
    			except youtube_dl.utils.DownloadError:
        			return "errore"
        		else:
                                info_dict = ydl.extract_info(link, download=False)
                                video_title = info_dict.get('title', None)
				video_id = info_dict.get('id', None)
				dir = os.path.dirname(__file__)
				fil = (video_title + '-'+ video_id + '.mp3')
				return send_from_directory(dir,fil, as_attachment=True, attachment_filename="{}{}".format(video_title.encode('utf-8'),'.mp3'))	
				            						
